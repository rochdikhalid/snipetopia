# Sprint 01 (May 29 - Jun 05)

* [] R2: Design the database structure for `snipetopia` using an ERD
* [] R0.5: Review and refine the ERD based on feedback and project requirements
* [] R0.2: Set up an empty ASP.net project and run it locally
* [] R1: Understand the project structure and how it works
* [] R0.2: Create a new PostgreSQL database for `snipetopia`
* [] R0.2: Connect the ASP.net project to the PostgreSQL database
* [] R0.1: Include the ASP.net application in a folder called **server**